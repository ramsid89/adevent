## Steps to build the project:
### prerequisite:
* Install JAVA 8.
* install maven 3.5+.

### steps to run project.
1. Checkout this project by doing git clone to your local machine.
2.  Run `mvn scala:run -DmainClass=com.gumgum.adevent.adevents`.
3. Output folder `output` will be created under the folder `adevent` with success and part file.
4. Open part file which is JSON file to view the output.

