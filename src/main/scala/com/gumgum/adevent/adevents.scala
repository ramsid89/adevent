package com.gumgum.adevent

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.lead
import org.apache.spark.sql.functions.when
import org.apache.spark.sql.functions.col
  /**
	 * Transforms ad events data to find user journey through each web page by calculating 'NextPageURL' 
	 */
object adevents {
  def main(args: Array[String]) {
    val sparkSession = SparkSession.builder().master("local").appName("adevent").getOrCreate() //creates spark session
    var df = sparkSession.read.format("json").load("input.txt").select("id","timestamp","type","visitorId","pageUrl")
    var filteredDf = df.filter("visitorId is not null") //load input data and check for valid visitor Id
    // sorts visitorId, timeStamp and populate the next pageURL.
    var dfNextPageUrl = filteredDf.withColumn("nextPageUrl", when(col("visitorId") === lead(col("visitorId"), 1).over(Window.orderBy("visitorId", "timestamp")), lead(col("pageUrl"), 1).over(Window.orderBy("visitorId", "timestamp"))).otherwise (null))
    dfNextPageUrl.write.format("json").mode("overwrite").option("header", true).save("output") //write output as json
    sparkSession.stop()
  }
}